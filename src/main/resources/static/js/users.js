$(document).ready(function(){
    // Activate tooltip
    $('[data-toggle="tooltip"]').tooltip();

    var emailRegex = new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
    var dateRegex = new RegExp("^([0-2][0-9]|(3)[0-1])(\\/)(((0)[0-9])|((1)[0-2]))(\\/)\\d{4}$");

    $(".names-error").hide();
    $(".email-error").hide();
    $(".date-error").hide();

    // Create User
    $("#add-user").click(function () {
        $(".names-error").hide();
        $(".email-error").hide();
        $(".date-error").hide();
        var firstName = $("#addFirstName").val();
        var lastName = $("#addLastName").val();
        var email = $("#addEmail").val();
        var dateOfBirth = $("#addDateOfBirth").val();

        var errorMsg = false;

        if(!firstName || !lastName) {
            $(".name-error").show();
            errorMsg = true;
        }
        if(!emailRegex.test(email)) {
            $(".email-error").show();
            errorMsg = true;
        }
        if(!dateRegex.test(dateOfBirth)) {
            $(".date-error").show();
            errorMsg = true;
        }

        var user = {
            firstName: firstName,
            lastName: lastName,
            email: email,
            dateOfBirth: dateOfBirth
        };

        if(!errorMsg) {
            $.ajax({
                url: "/create",
                type: 'POST',
                dataType: 'json',
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data: user,
                success: function () {
                    history.go(0);
                    location.reload();
                },
                error: function () {
                    history.go(0);
                    location.reload();
                }
            });
        }
    });


    // Delete User
    $(".delete").click(function () {
        var userId = $(this).attr('alt');
        $(".delete-user").attr('alt', userId);
    });

    $(".delete-user").click(function () {
        var userId = $(this).attr('alt');
        $.get("/delete/?id=" + userId);

        window.location.replace("/");
        history.go(0);
        location.reload();
    });


    // Update User
    $(".edit").click(function () {
        userId = $(this).attr('alt');
        $("#update-user").attr('alt', userId);

        $.get("/find/user?id=" + userId, function (data) {
           var user = data.split(",");
            $("#updateFirstName").val(user[1].split("=")[1]);
            $("#updateLastName").val(user[2].split("=")[1]);
            $("#updateEmail").val(user[3].split("=")[1]);
            $("#updateDate").val(user[4].split("=")[1]);
        });
    });


    $("#update-user").click(function () {
        $(".names-error").hide();
        $(".email-error").hide();
        $(".date-error").hide();
        var firstName = $("#updateFirstName").val();
        var lastName = $("#updateLastName").val();
        var email = $("#updateEmail").val();
        var dateOfBirth = $("#updateDate").val();

        var errorMsg = false;

        if(!firstName || !lastName) {
            $(".names-error").show();
            errorMsg = true;
        }
        if(!emailRegex.test(email)) {
            $(".email-error").show();
            errorMsg = true;
        }
        if(!dateRegex.test(dateOfBirth)) {
            $(".date-error").show();
            errorMsg = true;
        }

        var user = {
            firstName: firstName,
            lastName: lastName,
            email: email,
            dateOfBirth: dateOfBirth
        };

        if(!errorMsg) {
            var userId = $(this).attr('alt');
            $.get("/delete/?id=" + userId);

            $.ajax({
                url: "/create",
                type: 'POST',
                dataType: 'json',
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data: user,
                success: function () {
                    history.go(0);
                    location.reload();
                },
                error: function () {
                    history.go(0);
                    location.reload();
                }
            });
        }
    });
});