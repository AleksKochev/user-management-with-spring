package demo.users.util;

import demo.users.models.User;

import java.util.Comparator;

public class UserComparator {

    private UserComparator() {
    }

    public static Comparator<User> firstNameComparator() {
        return (user, user2) -> user.getFirstName().compareTo(user2.getFirstName());
    }

    public static Comparator<User> lastNameComparator() {
        return (user, user2) -> user.getLastName().compareTo(user2.getLastName());
    }

    public static Comparator<User> emailComparator() {
        return (user, user2) -> user.getEmail().compareTo(user2.getEmail());
    }

    public static Comparator<User> dateComparator() {
        return (user, user2) -> user.getDateOfBirth().compareTo(user2.getDateOfBirth());
    }
}
