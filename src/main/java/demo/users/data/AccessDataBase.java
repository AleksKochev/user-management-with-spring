package demo.users.data;

import demo.users.models.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AccessDataBase extends MongoRepository<User,String> {

}
