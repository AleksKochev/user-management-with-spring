package demo.users.controllers;

import demo.users.data.AccessDataBase;
import demo.users.models.User;
import demo.users.util.UserComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Stream;

@Controller
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private AccessDataBase dbConnector;

    @GetMapping("/")
    public String list(@RequestParam(value = "sortBy", defaultValue = "firstName") String param, Model model) {
        List<User> users = dbConnector.findAll();

        if(users.isEmpty()) {
            model.addAttribute("users", Collections.EMPTY_LIST);
            model.addAttribute("count", "0");
            return "users";
        }

        if (param == null || param.equals("firstName"))  {
            users.sort(UserComparator.firstNameComparator());
        } else if (param.equals("lastName")) {
            users.sort(UserComparator.lastNameComparator());
        } else if (param.equals("email")) {
            users.sort(UserComparator.emailComparator());
        } else if (param.equals("date")) {
            users.sort(UserComparator.dateComparator());
        }

        model.addAttribute("users", users);
        model.addAttribute("count", String.valueOf(dbConnector.count()));
        return "users";
    }

    @PostMapping("/create")
    public ResponseEntity<User> create(@RequestBody String user) {
        dbConnector.save(parseRequest(user));
        return new ResponseEntity<User>(HttpStatus.OK);
    }

    @GetMapping("/find/user")
    @ResponseBody
    public String find(@RequestParam(value = "id") String id) {
        User user = dbConnector.findById(id).get();
        return user.toString();
    }

    private User parseRequest(String user) {
        String processed = user.replaceFirst("%40", "@");
        processed = processed.replaceAll("%2F", "/");

        String[] request = processed.split("&");
        String firstName = request[0].split("=")[1];
        String lastName = request[1].split("=")[1];
        String email = request[2].split("=")[1];
        String dateOfBirth = request[3].split("=")[1];

        return new User(firstName, lastName, email, processDate(dateOfBirth));
    }

    @RequestMapping("/delete")
    @ResponseBody
    public String delete(@RequestParam(value = "id") String id) {
        dbConnector.deleteById(id);
        return "OK";
    }

    @RequestMapping("/generate")
    @ResponseBody
    public String generate() {

        Stream<String> data;
        try {
            data = Files.lines(Paths.get("E:\\Exceeding Technologies\\Modules\\users\\src\\main\\resources\\dummyData.txt"));
        } catch (IOException e) {
            LOGGER.error("Unable to find file: dummyData.txt", e);
            return "Unable to find file: dummyData.txt";
        }
        if (data == null) {
            return "REQUEST FAILED";
        } else {
            data.map(x -> x.split(","))
                    .filter(x -> x.length == 4)
                    .forEach(x -> dbConnector.save(new User(x[0], x[1], x[2], processDate(x[3]))));

            data.close();
            return "OK";
        }
    }

    private Date processDate(String date) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date dateOfBirth = null;
        try {
             dateOfBirth = format.parse(date);
        } catch (ParseException e) {
            LOGGER.error("Could not parse birth date: + " + date, e);
        }

        return dateOfBirth;
    }

    @RequestMapping("/delete/all")
    @ResponseBody
    public String deleteAll() {
        dbConnector.deleteAll();
        return "OK";
    }
}
