package demo.users.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.data.annotation.Id;

import java.text.SimpleDateFormat;
import java.util.Date;

public class User {

    @Id
    private String id;

    private String firstName;
    private String lastName;
    private String email;

    @JsonFormat(pattern="dd/MM/yyyy")
    private Date dateOfBirth;

    public User() {
    }

    public User(String firstName, String lastName, String email, Date dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public String toString() {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/YYYY");
        return String.format(
                "id=%s, firstName=%s, lastName=%s, email=%s, dateOfBirt=%s",
                id, firstName, lastName, email, format.format(dateOfBirth));
    }

}
